# CoinStack SDK for Meteor

The official CoinStack SDK for Meteor.

## Usage
### Registration
Appropriate credentials must be created prior to using CoinStack SDK for Meteor; credentials can be created using CoinStack dashboard (https://www.blocko.io/)
### Initialization
You can provide credentials through constructor or environment varaiables
```sh
export COINSTACK_ACCESS_KEY="YOUR_COINSTACK_ACCESS_KEY"
export COINSTACK_SECRET_KEY="YOUR_COINSTACK_SECRET_KEY"
```
```js
var client = new CoinStack()
```
or
```js
var accessKey = "YOUR_COINSTACK_ACCESS_KEY"
var secretKey = "YOUR_COINSTACK_SECRET_KEY"
var client = new CoinStack(accessKey, secretKey)
```
### API Usage
#### Server
```js
// get best block 
var status = client.getBlockchainStatusSync()
console.log(status.best_block_hash, best_height)

// check balance
var balance = client.getBalanceSync("YOUR_BLOCKCHAIN_ADDRESS")
console.log(balance);

// creating tx
var txBuilder = client.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS1", Client.Math.toSatoshi("0.01"))
txBuilder.addOutput("TO_ADDRESS2", Client.Math.toSatoshi("0.03"))
txBuilder.setInput("MY_WALLET_ADDRESS");
var tx = client.buildTransactionSync(txBuilder);
tx.sign("MY_PRIVATE_KEY_WIF")
var rawTx = tx.serialize()

// send tx
try {
    client.sendTransactionSync(rawTx)
} catch (e) {
    console.log("failed to send tx");
}
```
#### Client
```js
// get best block 
client.getBlockchainStatus(function(err, status) {
    console.log(status.best_block_hash, status.best_height);
});

// check balance
client.getBalance("YOUR_BLOCKCHAIN_ADDRESS", function(err, balance) {
    console.log(balance);
});

// creating tx
var txBuilder = client.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS1", Client.Math.toSatoshi("0.01"))
txBuilder.addOutput("TO_ADDRESS2", Client.Math.toSatoshi("0.03"))
txBuilder.setInput("MY_WALLET_ADDRESS");
txBuilder.buildTransaction(function(err, tx) {
    tx.sign("MY_PRIVATE_KEY_WIF")
    var rawTx = tx.serialize()
    // send tx
    client.sendTransaction(rawTx, function(err) {
        if (null != err) {
            console.log("failed to send tx");
        }
    );
})
```
## Further Information
Visit https://www.blocko.io/ for registration and management.

## License
Licensed under the MIT License.