var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
var client = new CoinStack(apiKey, secretKey)

Tinytest.add('client creation', function(test) {
    test.isNotNull(client)

    var status = client.getBlockchainStatusSync()
    test.isNotNull(status)
});

Tinytest.add('blockchain status', function(test) {
    var status = client.getBlockchainStatusSync()
    test.isNotNull(status)
});

Tinytest.add('getTx', function(test) {
    var tx = client.getTransactionSync("001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31")
    test.isNotNull(tx)
});

Tinytest.add('getBlock', function(test) {
    var balance = client.getBalanceSync("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh")
    test.isNotNull(balance)
});

Tinytest.add('getTransactions', function(test) {
    var txs = client.getTransactionsSync("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh")
    test.isNotNull(txs)
});

Tinytest.add('getUtxos', function(test) {
    var uxtos = client.getUnspentOutputsSync("17tz7JRkxkzhkCnqtnyYXu8sk4E8QwZUqh")
    test.isNotNull(uxtos)
});

Tinytest.add('sendTx', function(test) {
    var exceptionThrown = false
    try {
        client.sendTransactionSync("010000000124398225cf3d515a7ef7e816c37cbfd1cae9e01b401b90192c4dd479d23e7eab000000006b483045022100a8b331d506e265e79feb535a51dd5fbcd2724f0f6a1482cbea38d772ceae4e8c02206d7ee4bf2af3f8289310a09cc9760df6ae4768e24238cc13aee1739b837900ea012102ce3b0c53a06262e2a64e0639f2901447c2288ab437b5317fe05848e92a2ba25fffffffff0216120100000000001976a91415aad25727498a360e92eeb96db26f55fb38edcb88ac10270000000000001976a914abf0db3809c8ae1697f067a5c92171fd6ca3aaa988ac00000000")
    } catch (e) {
    	exceptionThrown = true
    }
    test.isTrue(exceptionThrown)
});