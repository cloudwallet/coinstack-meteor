Package.describe({
    name: 'shepelt:coinstack',
    version: '1.1.21',
    summary: 'CoinStack Blockchain SDK for JavaScript',
    git: '',
    documentation: 'README.md'
});

Npm.depends({
    'coinstack-sdk-js': '1.1.19',
});

Package.onUse(function(api) {
    api.versionsFrom('1.2');
    api.addFiles('coinstack.js', ['server']);
    api.addFiles(['coinstack-1.1.21.min.js', 'bitcoinjs.min.1.5.7.js', 'coinstack.browserify.js'], 'client');
    api.export("CoinStack", ['server']);
    api.export("coinstack_user_set_bitcoin", ['client']);
});

Package.onTest(function(api) {
    api.use(['tinytest', 'test-helpers'], ['server']);
    api.addFiles(['coinstack.js', 'coinstack_test.js'], ['server']);
});
