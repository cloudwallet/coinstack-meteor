CoinStack = Npm.require('coinstack-sdk-js')

CoinStack.prototype.getBlockchainStatusSync = Meteor.wrapAsync(CoinStack.prototype.getBlockchainStatus)
CoinStack.prototype.getBlockSync = Meteor.wrapAsync(CoinStack.prototype.getBlock)
CoinStack.prototype.getTransactionSync = Meteor.wrapAsync(CoinStack.prototype.getTransaction)
CoinStack.prototype.getTransactionsSync = Meteor.wrapAsync(CoinStack.prototype.getTransactions)
CoinStack.prototype.getBalanceSync = Meteor.wrapAsync(CoinStack.prototype.getBalance)
CoinStack.prototype.getUnspentOutputsSync = Meteor.wrapAsync(CoinStack.prototype.getUnspentOutputs)
CoinStack.prototype.sendTransactionSync = Meteor.wrapAsync(CoinStack.prototype.sendTransaction)
CoinStack.prototype.stampDocumentSync = Meteor.wrapAsync(CoinStack.prototype.stampDocument)
CoinStack.prototype.getStampSync = Meteor.wrapAsync(CoinStack.prototype.getStamp)
CoinStack.prototype.buildTransactionSync = Meteor.wrapAsync(function(txBuilder, callback) {
	txBuilder.buildTransaction(callback)
})
